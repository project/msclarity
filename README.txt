
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------
Clarity is a cutting-edge behavioral analysis tool that helps you understand 
users interaction with your website. By using Clarity's robust analysis tools,
you can enhance your website for your clients and your business.

Requirements
============

* Microsoft Clarity user account

Installation
============
Copy the 'msclarity' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
In the settings page enter your Clarity ID.

All pages will now have the required JavaScript added to the
<head> section.

Page specific tracking
======================
The default is set to "Add to every page except the listed pages". By
default the following pages are listed for exclusion:

admin
admin/*
batch
node/add*
node/*/*
user/*/*

These defaults are changeable by the website administrator or any other
user with 'Administer Microsoft Clarity' permission.

Like the blocks visibility settings in Drupal core, there is a choice for
"Add if the following PHP code returns TRUE." Sample PHP snippets that can be
used in this textarea can be found on the handbook page "Overview-approach to
block visibility" at https://drupal.org/node/64135.

CONFIGURATION
-------------

Users in roles with the 'Administer Microsoft Clarity' permission will be able
to manage the module settings and containers for the site. Configure permissions
as usual at:

 * Administration » People » Permissions
 * admin/people/permissions

From the Microsoft Clarity settings page, manage the clarityID to be inserted on a
page response. See:

 * Administration » Configuration » System » Microsoft Clarity
 * admin/config/system/msclarity


MAINTAINERS
-----------

Current maintainer:

 * Gopinath Dhayalan (https://www.drupal.org/u/gopinath-dhayalan)