<?php

/**
 * @file
 * Admin page callbacks for the Microsoft Clarity module.
 */

/**
 * Form constructor for the Microsoft Clarity administration form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function msclarity_admin_form() {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['account']['msclarity_id'] = array(
    '#title' => t('Clarity ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('msclarity_id', ''),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Clarity ID is unique to each site you want to track separately. To get a Clarity ID, <a href="@msclarity">register your site with Microsoft Clarity</a>, or if you already have registered your site, go to your Microsoft Clarity Settings page to get the Clarity ID from tracking code. <a href="@clarityid">Find more information in the documentation</a>.', array('@msclarity' => 'https://clarity.microsoft.com/', '@clarityid' => url('https://clarity.microsoft.com/projects'))),
  );

  // Visibilty settings.
  $form['visibility_title'] = array(
    '#type' => 'item',
    '#title' => t('Visibility settings'),
  );
  $form['visibility'] = array(
    '#type' => 'vertical_tabs',
  );
  // Page specific visibility configurations.
  $php_access = user_access('use PHP code for tracking visibility');
  $visibility = variable_get('_msclarity_visibility_pages', 0);
  $pages = variable_get('msclarity_pages', MICROSOFTCLARITY_PAGES);

  $form['visibility']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2 && !$php_access) {
    $form['visibility']['page_vis_settings'] = array();
    $form['visibility']['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['visibility']['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $php_access) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['visibility']['page_vis_settings']['msclarity_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['visibility']['page_vis_settings']['msclarity_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  // Per-role visibility..
  $form['visibility']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
  );
  $form['visibility']['role_vis_settings']['msclarity_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('msclarity_visibility_roles', 0),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['visibility']['role_vis_settings']['msclarity_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('msclarity_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
  );
  return system_settings_form($form);
}
